# Hie there! let us learn how to create a database **using phpMyAdmin******! Interesting right! #

# 1. Open **phpMyAdmin** and select create database. this will enable you to give a name to your database, for example "bakery" #

![Скриншот 2019-05-28 23.06.31.png](https://bitbucket.org/repo/eyk6dd5/images/300995067-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202019-05-28%2023.06.31.png)

# 2. Create different tables that show different specifications of data that make up your database altogether. In my case this includes customers and/or workers. #

![Скриншот 2019-05-28 23.13.30(2).png](https://bitbucket.org/repo/eyk6dd5/images/2266454933-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202019-05-28%2023.13.30(2).png)

# After creating all your tables make sure each one looks like this. #

![Скриншот 2019-05-28 23.14.06.png](https://bitbucket.org/repo/eyk6dd5/images/2538613586-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202019-05-28%2023.14.06.png)

## 3. On the main table select more then choose designer so you can come up with a table. ##

![Скриншот 2019-05-28 23.29.21.png](https://bitbucket.org/repo/eyk6dd5/images/107456604-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202019-05-28%2023.29.21.png)

### 4. After all the steps then choose the option form relationship. this enables to show the relationship between the data in your database. ###

![Скриншот 2019-05-28 23.29.56.png](https://bitbucket.org/repo/eyk6dd5/images/828078226-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202019-05-28%2023.29.56.png)

### 5. Connect showing primary keys and data there is in common. That is all! In the end it has to look something like this; ###



### I hope this was helpful. Good luck! ###